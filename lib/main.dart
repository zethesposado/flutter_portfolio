import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_markdown/flutter_markdown.dart';


void main() {
    runApp(MyApp());
}

class MyApp extends StatelessWidget {

    @override 
    Widget build(BuildContext context) {
        return MaterialApp(
            home: Scaffold(
                body: Center(
                    child: MyProfile(),
                )
            ),
        );
    }
}

class DottedText extends Text {
  const DottedText(String data, {
    Key? key,
    TextStyle? style,
    TextAlign? textAlign,
    TextDirection? textDirection,
    Locale? locale,
    bool? softWrap,
    TextOverflow? overflow,
    double? textScaleFactor,
    int? maxLines,
    String? semanticsLabel,
  }) : super(
    '\u2022 $data',
    key: key,
    style: style,
    textAlign: textAlign,
    textDirection: textDirection,
    locale: locale,
    softWrap: softWrap,
    overflow: overflow,
    textScaleFactor: textScaleFactor,
    maxLines: maxLines,
    semanticsLabel: semanticsLabel,);
}

class MyProfile extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Center(
                child: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                                Color(0xff05d6d9),
                                Color(0xfff907fc)
                            ]
                        )
                    ),
                    child: Center(
                        child: SingleChildScrollView(
                            child: Column(
                            children: [
                                displayPhoto(),
                                socMedBtn(),
                                infoCard(),
                                workExp(),
                                skills(),
                                interest(),
                                contactInfo(),
                                empty()
                            ],
                        ))
                    ),
                ),
            )
        );
    }

    // Container background() {
    //     return Container(
    //         child: Image.asset('assets/images/scenery-bg.jpg',
    //             fit: BoxFit.cover,
    //             height: 300.0,
    //         ),
    //         constraints: BoxConstraints.expand(height: 300.0),
    //     );
    // }

    // Container gradient() {
    //     return Container(
    //         margin: EdgeInsets.only(top: 200.0),
    //         height: 110.0,
    //         decoration: BoxDecoration(
    //             gradient: LinearGradient(
    //                 colors: [
    //                     Color(0x00736AB7),
    //                     Color(0xFF736AB7),
    //                 ],
    //             stops: [0.0, 0.9],
    //             begin: const FractionalOffset(0.0, 0.0),
    //             end: const FractionalOffset(0.0, 1.0), 
    //             )
    //         ),
    //     );
    // }

    Container displayPhoto() {
        return Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
                children: [
                    Stack(
                        alignment: Alignment.center,
                        children: [
                            Positioned(
                                child: CircleAvatar(
                                    radius: 82,
                                    backgroundColor: Colors.white,
                                    child: CircleAvatar(
                                        radius: 75,
                                        backgroundImage: AssetImage('assets/images/me2.jpg')
                                ),
                            )),
                        ],
                    )
                ],
            ),
        );
    }

    Container socMedBtn() {

        return Container(
            margin: EdgeInsets.only(left: 50, right: 50, top: 20),
            alignment: FractionalOffset.center,
            width: double.infinity,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                    GFIconButton(
                        onPressed: () async {
                            const url = 'https://www.facebook.com/zthespsd.esc';
                            if (await canLaunch(url)) {
                                await launch(url);
                            } else {
                                throw Exception(e);
                            }
                        },
                        color: Color.fromRGBO(66 , 103, 178, 1),
                        icon: Icon(Icons.facebook),
                        shape: GFIconButtonShape.pills,
                    ),
                    SizedBox(width: 50),
                    GFIconButton(
                        onPressed: () async {
                            const url = 'https://www.instagram.com/zthes.psd_file/';
                            if (await canLaunch(url)) {
                                await launch(url);
                            } else {
                                throw Exception(e);
                            }
                        },
                        color: Colors.redAccent.shade700,
                        icon: FaIcon(FontAwesomeIcons.instagram),
                        shape: GFIconButtonShape.pills,
                    ),
                    SizedBox(width: 50),
                    GFIconButton(
                        onPressed: () async {
                            const url = 'https://www.linkedin.com/in/john-nazareth-esposado';
                            if (await canLaunch(url)) {
                                await launch(url);
                            } else {
                                throw Exception(e);
                            }
                        },
                        color: Color.fromRGBO(0, 119, 181, 1),
                        icon: FaIcon(FontAwesomeIcons.linkedin),
                        shape: GFIconButtonShape.pills,
                    ),

                ],

            )
        );
    }

    Container infoCard() {
        return Container(
            padding: EdgeInsets.only(top: 16.0),
            width: double.infinity,
            child: Column(
                children: [
                    Stack(
                        alignment: Alignment.center,
                        children: [
                            Positioned(
                                child: Container(
                                    // height: 200.0,
                                    width: 950.0,
                                    margin: EdgeInsets.all(24.0),
                                    child: Card(
                                        elevation: 8.0,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(8.0)
                                        ),
                                        child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                                Padding(
                                                    padding: const EdgeInsets.all(16.0),
                                                    child: Text('John Nazareth R Esposado', 
                                                        style: TextStyle(
                                                            fontSize: 20.0,
                                                            fontWeight: FontWeight.bold
                                                        ),
                                                    ),
                                                ),
                                                Padding(
                                                    padding: const EdgeInsets.all(16.0),
                                                    child: Text('A career shifter, worked as a Data Analyst from a BPO company and now an aspiring Front-End Developer. Currently a trainee in FFUF Manila that uses Git, Dart and Flutter.', textAlign: TextAlign.center, )
                                                )
                                            ],
                                        ),
                                    )
                                ),
                            )
                        ],
                    )
                ],
            ),
        );
    }

    Container workExp() {
        return Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white,
                    width: 2
                ),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 25.0, right: 25.0),
            width: 900,
            child: Theme(
                data: ThemeData(unselectedWidgetColor: Colors.white, accentColor: Colors.white, textTheme: TextTheme(
                    subtitle1: TextStyle(color:Colors.white),
                )),
                child: ExpansionTile(
                    tilePadding: EdgeInsets.all(8.0),
                    title: Text('Work Experience', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                    leading: FaIcon(FontAwesomeIcons.suitcase, color: Colors.white,),
                        children: [
                            ListTile(
                                title: DottedText('Flutter Developer Trainee', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text('FFUF Manila Inc. (2021 - Present)', style: TextStyle(color: Colors.white)),
                            ),
                            ListTile(
                                title: DottedText('Platform Experience New Associate', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text('Accenture (2018 - 2021)', style: TextStyle(color: Colors.white)),
                            ),
                        ],
                ), 
            ),
        );
    }

    Container skills() {
        return Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white,
                    width: 2
                ),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            width: 900,
            child: Theme(
                data: ThemeData(unselectedWidgetColor: Colors.white, accentColor: Colors.white, textTheme: TextTheme( 
                    subtitle1: TextStyle(color: Colors.white)
                )),
                child: ExpansionTile(
                    tilePadding: EdgeInsets.all(8.0),

                    title: Text('Skills', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                    leading: FaIcon(FontAwesomeIcons.tools, color: Colors.white,),
                        children: [
                            ListTile(
                                title: DottedText('Dev: HTML, CSS, JavaSript, PHP, Git, Dart, Flutter', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Database: MS SQL, SQL', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Design: Illustrator, Photoshop, Autodesk Sketchbook', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Personal: Cooperative, Flexible, Adaptability', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                        ],
                ), 
            ),
        );
    }

    Container interest() {
        return Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white,
                    width: 2
                ),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            width: 900,
            child: Theme(
                data: ThemeData(unselectedWidgetColor: Colors.white, accentColor: Colors.white, textTheme: TextTheme( 
                    subtitle1: TextStyle(color: Colors.white)
                )),
                child: ExpansionTile(
                    tilePadding: EdgeInsets.all(8.0),

                    title: Text('Interests', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                    leading: FaIcon(FontAwesomeIcons.solidHeart , color: Colors.white,),
                        children: [
                            ListTile(
                                title: DottedText('Digital Art', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Web Design and Development', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Anime & Sci-fi movies ', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                            ListTile(
                                title: DottedText('Gaming', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                            ),
                        ],
                ), 
            ),
        );
    }

    Container contactInfo() {
        return Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white,
                    width: 2
                ),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            width: 900,
            child: Theme(
                data: ThemeData(unselectedWidgetColor: Colors.white, accentColor: Colors.white, textTheme: TextTheme( 
                    subtitle1: TextStyle(color: Colors.white)
                )),
                child: ExpansionTile(
                    tilePadding: EdgeInsets.all(8.0),

                    title: Text('Contact Info', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                    leading: FaIcon(FontAwesomeIcons.info , color: Colors.white,),
                        children: [
                            ListTile(
                                title: DottedText('E-mail', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text('zethesposado@gmail.com', style: TextStyle(color: Colors.white)),
                            ),
                            ListTile(
                                title: DottedText('Phone number', 
                                    style: TextStyle(
                                        fontSize: 16.0, 
                                        fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text('0975-4**-****', style: TextStyle(color: Colors.white)),
                            ),
                        ],
                ), 
            ),
        );
    }

    Container empty() {
        return Container(
            child: Padding(
                padding: EdgeInsets.all(40.0)),
        );
    }
}
